/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 16:01:53 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <string.h>

int	ft_strcmp(char *s1, char *s2);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
}

int	test01(void)
{
	char	*s1 = "ssdjf1gs";
	char	*s2 = "ssdjf1gs";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strcmp(s1, s2) == ft_strcmp(s1, s2));
}

int	test02(void)
{
	char	*s1 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	char	*s2 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strcmp(s1, s2) == ft_strcmp(s1, s2));
}

int	test03(void)
{
	char	*s1 = "1234";
	char	*s2 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strcmp(s1, s2) == ft_strcmp(s1, s2));
}

int	test04(void)
{
	char	*s1 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	char	*s2 = "1234";
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strcmp(s1, s2) == ft_strcmp(s1, s2));
}

int	test05(void)
{
	char	s1[5];
	char	s2[5];

	s1[0] = '4';
	s1[1] = '4';
	s1[2] = '4';
	s1[3] = '4';
	s1[4] = '4';
	s2[0] = '4';
	s2[1] = 's';
	s2[2] = '4';
	s2[3] = '4';
	s2[4] = '4';
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strcmp(s1, s2) == ft_strcmp(s1, s2));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	return (0);
}
