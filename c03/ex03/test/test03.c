/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/26 08:01:44 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

char	*ft_strncat(char *dest, char *src, unsigned int nb);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{
	char	*src = "WorldWorldWorld\0";
	char	dest[12] = "Hello ";
	char	dest2[12] = "Hello ";

	write(1, strncat(dest, src, 5), 12);
	write(1, " : ", 3);
	write(1, ft_strncat(dest2, src, 5), 12);
	return (strcmp(dest, dest2) == 0);
}

int	test02(void)
{
	char	src[6] = "";
	char	dest[12] = "Hello";
	char	dest2[12] = "Hello";

	write(1, strncat(dest, src, 0), 12);
	write(1, " : ", 3);
	write(1, ft_strncat(dest2, src, 0), 12);
	return (strcmp(dest, dest2) == 0);
}

int	test03(void)
{
	char	*src = " and good morning to you all";
	char	*src2 = " fellow insignificant human!";
	char	dest[60] = "Hello";
	char	dest2[60] = "Hello";

	write(1, strncat(strncat(dest, src, 17), src2, 0), 60);
	write(1, " : ", 3);
	write(1, ft_strncat(ft_strncat(dest2, src, 17), src2, 0), 60);
	return (strcmp(dest, dest2) == 0);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	return (0);
}
