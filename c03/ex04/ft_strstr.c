/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 08:32:51 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 10:48:07 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Problem is when to_find is empty

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		j;
	char	*p;

	i = 0;
	j = 0;
	p = 0;
	if (*to_find == 0)
		return (str);
	while (str[i] && to_find[j])
	{
		if (str[i++] == to_find[j])
		{
			if (p == 0)
				p = &str[i - 1];
			j++;
			continue ;
		}
		p = 0;
		j = 0;
	}
	if (to_find[j] == 0)
		return (p);
	return (0);
}
