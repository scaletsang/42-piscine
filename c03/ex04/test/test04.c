/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/27 14:30:10 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

char	*ft_strstr(char *str, char *to_find);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{
	char	str[14] = "sdhftgsdlgabc\0";
	char	sub[4] = "abc\0";

	write(1, strstr(str, sub), 3);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 3);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test02(void)
{
	char	str[14] = "abcsdhftgsdlg\0";
	char	sub[4] = "abc\0";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test03(void)
{
	char	str[14] = "hkssabctgsdlg\0";
	char	sub[4] = "abc\0";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test04(void)
{
	char	str[14] = "hkssbctgsdlg\0";
	char	sub[4] = "abc\0";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test05(void)
{
	char	str[14] = "hkssbctgsabc\0";
	char	sub[4] = "abc\0";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test06(void)
{
	char	str[] = "hkssbctgsabc\0";
	char	*sub = "";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test07(void)
{
	char	str[] = "";
	char	*sub = "abc\0";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test08(void)
{
	char	str[] = "";
	char	*sub = "";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test09(void)
{
	char	*str = 0;
	char	*sub = "";

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	test10(void)
{
	char	*str = "";
	char	*sub = 0;

	write(1, strstr(str, sub), 10);
	write(1, " ", 1);
	write(1, ft_strstr(str, sub), 10);
	return (strstr(str, sub) == ft_strstr(str, sub));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	test(&test07);
	test(&test08);
	test(&test09);
	write(1, "Expecting Segmentation fault:\n", 31);
	test(&test10);
	return (0);
}
