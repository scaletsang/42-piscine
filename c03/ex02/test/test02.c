/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/26 07:49:16 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

char	*ft_strcat(char *dest, char *src);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
}

int	test01(void)
{
	char	*src = "World\0";
	char	dest[12] = "Hello ";
	char	dest2[12] = "Hello ";

	write(1, strcat(dest, src), 12);
	write(1, " : ", 3);
	write(1, ft_strcat(dest2, src), 12);
	return (strcmp(dest, dest2) == 0);
}

int	test02(void)
{
	char	src[6] = "";
	char	dest[12] = "Hello";
	char	dest2[12] = "Hello";

	write(1, strcat(dest, src), 12);
	write(1, " : ", 3);
	write(1, ft_strcat(dest2, src), 12);
	return (strcmp(dest, dest2) == 0);
}

int	test03(void)
{
	char	*src = " and good morning to you all";
	char	*src2 = " fellow insignificant human!";
	char	dest[60] = "Hello";
	char	dest2[60] = "Hello";

	write(1, strcat(strcat(dest, src), src2), 60);
	write(1, " : ", 3);
	write(1, ft_strcat(ft_strcat(dest2, src), src2), 60);
	return (strcmp(dest, dest2) == 0);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	return (0);
}
