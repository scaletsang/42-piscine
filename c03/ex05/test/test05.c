/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test05.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/11/06 13:42:29 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

unsigned int	n_strlcat(char *dst, const char *src, unsigned int dsize)
{
	const char		*odst = dst;
	const char		*osrc = src;
	unsigned int	n = dsize;
	unsigned int	dlen;

	/* Find the end of dst and adjust bytes left but don't go past end. */
	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = dsize - dlen;

	if (n-- == 0)
		return(dlen + strlen(src));
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return(dlen + (src - osrc));	/* count does not include NUL */
}

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	unsigned int	i;
	int				src_size;

	i = 0;
	src_size = 0;
	while (src[src_size])
	{
		src_size++;
	}
	if (dest == 0 || size <= 0)
	{
		return (src_size);
	}
	while (i < (size - 1))
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = 0;
	return (src_size);
}

void	print_number(int nb)
{
	char	digit;
	int		divider;

	divider = 1;
	while (nb / divider > 9)
	{
		divider *= 10;
	}
	while (divider >= 1)
	{
		digit = nb / divider + '0';
		write(1, &digit, 1);
		nb %= divider;
		divider /= 10;
	}
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-2147483648", 11);
		return ;
	}
	if (nb < 0)
	{
		nb = -nb;
		write(1, "-", 1);
	}
	print_number(nb);
	return ;
}

void	print_error(char *message, unsigned int val1, unsigned int val2, char *dest1, char *dest2)
{
	write(1, message, strlen(message));
	write(1, ": ", 2);
	write(1, "strlcat: ", 9);
	ft_putnbr(val1);
	write(1, " ", 1);
	write(1, dest1, strlen(dest1));
	write(1, " | ft_strlcat: ", 15);
	ft_putnbr(val2);
	write(1, " ", 1);
	write(1, dest2, strlen(dest2));
	return ;
}

int	general_test(char *dest, char *src, unsigned int size)
{
	unsigned int	i;
	unsigned int	val1;
	unsigned int	val2;
	int				is_ok;
	char			*dest1;
	char			*dest2;

	i = 0;
	is_ok = 1;
	dest1 = malloc(size);
	dest2 = malloc(size);
	while (i < 100)
	{
		ft_strlcpy(dest1, dest, strlen(dest) + 1);
		ft_strlcpy(dest2, dest, strlen(dest) + 1);
		val1 = strlcat(dest1, src, i);
		val2 = ft_strlcat(dest2, src, i);
		if (val1 != val2)
		{
			is_ok = 0;
			print_error("Return value is wrong", val1, val2, dest1, dest2);
			write(1, " | value of size: ", 18);
			ft_putnbr(i);
			write(1, "\n", 1);
		}
		else if (strcmp(dest1, dest2) != 0)
		{
			is_ok = 0;
			print_error("String concatenation is not done properly", val1, val2, dest1, dest2);
			write(1, " | value of size: ", 18);
			ft_putnbr(i);
			write(1, "\n", 1);
		}
		i++;
	}
	free(dest1);
	free(dest2);
	return (is_ok);
}

int	test01(void)
{
	char	*src = "World";
	char	dest[30] = "Hello \0";

	return (general_test(dest, src, 30));
}

int	test02(void)
{
	char	src[6] = "";
	char	dest[12] = "Hello\0";

	return (general_test(dest, src, 12));
}

int	test03(void)
{
	char	src[6] = "World\0";
	char	dest[12] = "Hello\0";

	return (general_test(dest, src, 12));
}

int	test04(void)
{
	char	*src = "World";
	char	*dest = "Hello";

	return (general_test(dest, src, 12));
}

int	test05(void)
{
	char	*src = "World";
	char	dest[5] = "Hello";

	return (general_test(dest, src, 5));
}

int	test06(void)
{
	char	*src = "World";
	char	*dest = "Hello";

	return (general_test(dest, src, 11));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	return (0);
}
