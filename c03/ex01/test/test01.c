/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 04:02:32 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

int	ft_strncmp(char *s1, char *s2, unsigned int n);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
}

int	general_test(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (i < 100)
	{
		if (!(strncmp(s1, s2, i) == ft_strncmp(s1, s2, i)))
		{
			return (0);
		}
		i++;
	}
	return (1);
}

int	test01(void)
{
	char	*s1 = "ssdjf1gs";
	char	*s2 = "ssdjf1gs";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strncmp(s1, s2, 8) == ft_strncmp(s1, s2, 8));
}

int	test02(void)
{
	char	*s1 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	char	*s2 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strncmp(s1, s2, 55) == ft_strncmp(s1, s2, 55));
}

int	test03(void)
{
	char	*s1 = "1234";
	char	*s2 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strncmp(s1, s2, 9) == ft_strncmp(s1, s2, 9));
}

int	test04(void)
{
	char	*s1 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	char	*s2 = "1234";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (strncmp(s1, s2, 9) == ft_strncmp(s1, s2, 9));
}

int	test05(void)
{
	char	s1[6] = "44444";
	char	s2[6] = "4s444";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test06(void)
{
	char	*s1 = "1234567890abcdefghijklmnopqrstuvwxyz!@#$^&*()_+{}:>/\\~`";
	char	*s2 = "1234";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test07(void)
{
	char	*s1 = "";
	char	*s2 = "1234";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test08(void)
{
	char	*s1 = "1234";
	char	*s2 = "";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test09(void)
{
	char	*s1 = "1234\201";
	char	*s2 = "1234\201";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test10(void)
{
	char	*s1 = "123";
	char	*s2 = "1234";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test11(void)
{
	char	*s1 = "1234";
	char	*s2 = "123";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	test12(void)
{
	char	*s1 = "1";
	char	*s2 = "123";

	write(1, s1, strlen(s1));
	write(1, " : ", 3);
	write(1, s2, strlen(s2));
	return (general_test(s1, s2));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	test(&test07);
	test(&test08);
	test(&test09);
	test(&test10);
	test(&test11);
	test(&test12);
	return (0);
}
