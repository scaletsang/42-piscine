# 42 Piscine exercises

I see these exercises as an entry point to various basic concepts in the C language. During the piscine, I write codes to test each exercises (from project shell00 to c04), quite detailly. However the process was quite slow and tedious. And so I wrote some small bash/shell scripts to smoothen my workflow for completing the tasks and testing codes. The folder helpers/ contained all these scripts. Inside, the init file is to source all the scripts in the anthony_commands/ folder, which contains all the the actual helper scripts.

After the Piscine, I felt the need to develop a testsuit to test any codes and programs. It will include a easy way to test a function's return value(pure tests), test a function's output to terminal (effectful tests), and an environment to test a seperate program (effectful tests on other programs). See the submodules c_testsuit: [Testsuit for C Programs](https://gitlab.com/scaletsang/c_testsuit).

Some thoughts about each projects:

| Project | Concepts | pdf |
| ---     | ---      | --- |
| shell00 | Git<br>Linux Commands: tar, chmod, diff & patch, find, file | [shell00.pdf](pdfs/shell00.pdf) |
| shell01 | Linux user groups, regular expression.<br>Linux commands: grep, sed, awk, tr | [shell01.pdf](pdfs/shell01.pdf) |
| c00 | write()<br>char as ASCII number<br>while loop, nested while loop, recursion | [c00.pdf](pdfs/c00.pdf) |
| c01 | Pointers<br>array manipulation (reversing and sorting arrays) | [c01.pdf](pdfs/c01.pdf) |
| c02 | String manipulations (comparing, copying and concantenating arrays)<br>casting | [c02.pdf](pdfs/c02.pdf) |
| c03 | Implementing most functions in string.h | [c03.pdf](pdfs/c03.pdf) |
| c04 | String to Int conversion in different bases | [c04.pdf](pdfs/c04.pdf) |
| c05 | Loops vs recursion<br>Recursion algorthm: backtracking | [c05.pdf](pdfs/c05.pdf) |
| c06 | C Programs | [c06.pdf](pdfs/c06.pdf) |
| c07 | malloc() & free()<br>Heap vs stack<br>Creating arrays based on input arrays on heap<br>String manipulations (spliting and joining strings) | [c07.pdf](pdfs/c07.pdf) |
| c08 | Header files<br>macros (namely preprocessors)<br>structs | [c08.pdf](pdfs/c08.pdf) |
| c09 | Makefile | [c09.pdf](pdfs/c09.pdf) |
| c10 | Accessing files: close(), open(), read(), write()<br>Files descriptor<br>Error handling & displaying errors: strerror, errno variable | [c10.pdf](pdfs/c10.pdf) |
| c11 | Higher-order functions acheived by passing functions pointers as arguments<br>Bubble sorts by passing in a comparision function pointer | [c11.pdf](pdfs/c11.pdf) |
| c12 | Data structure: linked list!!<br>All linked list manipulations (concat, push, prepend, reverse, foreach, remove_elem...)<br>Casting | [c12.pdf](pdfs/c12.pdf) |
| c13 | Data structure: Binary Tree<br>Tree manipulations<br>Binary search<br>Casting | [c13.pdf](pdfs/c13.pdf) |
