/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 19:12:57 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:18:56 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"
#include <stdlib.h>

void	set(int **map, int x, int y, int value)
{
	map[y][x] = value;
}

void	set_row(int **map, int *values, int row)
{
	int	x;

	x = 0;
	while (x < 4)
	{
		set(map, x, row, values[x]);
		x++;
	}
}

int	get(int **map, int x, int y)
{
	return (map[y][x]);
}

/*
 * Internal memory layout. Use helper functions above
 * and in main_adv.h to access.
 * 0 0 0 0
 * 0 0 0 0
 * 0 0 0 0
 * 0 0 0 0
 * t t t t
 * b b b b
 * l l l l
 * r r r r
 */
int	**create_map(void)
{
	int	**map;
	int	y;
	int	x;

	map = malloc(8 * sizeof(int *));
	y = 0;
	while (y < 8)
	{
		x = 0;
		map[y] = malloc(4 * sizeof(int));
		while (x < 4)
		{
			map[y][x] = -1;
			x++;
		}
		y++;
	}
	return (map);
}
