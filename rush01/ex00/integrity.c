/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integrity.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tluik <tluik@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 15:21:45 by bhagenlo          #+#    #+#             */
/*   Updated: 2021/10/23 17:59:30 by tluik            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"
#include "integrity.h"

int	check_cross(int **map, int x, int y)
{
	int	xgo;
	int	ygo;
	int	val;

	xgo = x + 1;
	ygo = y + 1;
	val = get(map, x, y);
	while (xgo < 4)
	{
		if (val == get(map, xgo, y))
			return (0);
		xgo++;
	}
	while (ygo < 4)
	{
		if (val == get(map, x, ygo))
			return (0);
		ygo++;
	}
	return (1);
}

int	check_integrity(int **map)
{
	int	x;
	int	y;

	x = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			if (!check_cross(map, x, y))
				return (0);
			y++;
		}
		x++;
	}
	return (1);
}
