/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proof.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 21:39:38 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:19:29 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROOF_H
# define PROOF_H 

void	set_proof_col(int **map, int col, int value, int top);

int		get_proof_col(int **map, int col, int top);

void	set_proof_row(int **map, int row, int value, int left);

int		get_proof_row(int **map, int row, int left);

int		set_proof(int **map, char *str);

#endif
