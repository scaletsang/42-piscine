/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integrity.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 15:35:31 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:11:02 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTEGRITY_H
# define INTEGRITY_H 

int	check_cross(int **map, int x, int y);

int	check_integrity(int **map);

#endif
