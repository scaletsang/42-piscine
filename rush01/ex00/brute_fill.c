/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brute_fill.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tluik <tluik@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 16:05:30 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 17:56:26 by tluik            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"
#include "checker.h"
#include "integrity.h"

int	brute_fill_rec(int **map, int x, int y)
{
	int	i;

	i = 1;
	if (x < 0)
	{
		x = 3;
		y--;
	}
	while (i <= 4)
	{
		set(map, x, y, i);
		if (check_cross(map, x, y))
		{
			if (x == 0 && y == 0)
				return (is_correct(map));
			if (brute_fill_rec(map, x - 1, y))
				return (1);
		}
		i++;
	}
	return (0);
}

int	brute_fill(int **map)
{
	return (brute_fill_rec(map, 3, 3));
}
