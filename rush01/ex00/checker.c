/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 20:14:15 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:29:38 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "map.h"
#include "proof.h"

int	is_row_correct(int **map, int row, int value, int left)
{
	int	x;
	int	last_v;
	int	proof_v;
	int	count;

	x = 0;
	last_v = 0;
	count = 0;
	while (x < 4)
	{
		if (left)
			proof_v = get(map, x, row);
		else
			proof_v = get(map, 3 - x, row);
		if (proof_v > last_v)
		{
			last_v = proof_v;
			count++;
		}
		x++;
	}
	return (count == value);
}

int	is_col_correct(int **map, int col, int value, int top)
{
	int	y;
	int	last_v;
	int	proof_v;
	int	count;

	y = 0;
	last_v = 0;
	count = 0;
	while (y < 4)
	{
		if (top)
			proof_v = get(map, col, y);
		else
			proof_v = get(map, col, 3 - y);
		if (proof_v > last_v)
		{
			last_v = proof_v;
			count++;
		}
		y++;
	}
	return (count == value);
}

int	is_entire_col_correct(int **map)
{
	int	i;
	int	t;
	int	proof;

	t = 0;
	while (t < 2)
	{
		i = 0;
		while (i < 4)
		{
			proof = get_proof_col(map, i, t);
			if (!is_col_correct(map, i, proof, t))
				return (0);
			i++;
		}
		t++;
	}
	return (1);
}

int	is_entire_row_correct(int **map)
{
	int	i;
	int	l;
	int	proof;

	l = 0;
	while (l < 2)
	{
		i = 0;
		while (i < 4)
		{
			proof = get_proof_row(map, i, l);
			if (!is_row_correct(map, i, proof, l))
				return (0);
			i++;
		}
		l++;
	}
	return (1);
}

int	is_correct(int **map)
{
	if (!is_entire_col_correct(map))
		return (0);
	if (!is_entire_row_correct(map))
		return (0);
	return (1);
}
