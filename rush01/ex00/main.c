/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 19:15:04 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:29:38 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "map.h"
#include "brute_fill.h"
#include "printer.h"
#include "proof.h"

void	free_map(int **map)
{
	int	i;

	i = 0;
	while (i < 8)
	{
		free(map[i]);
		i++;
	}
	free(map);
}

int	main(int argc, char **argv)
{
	int	**map;

	if (argc != 2)
	{
		write(1, "Error\n", 6);
		return (1);
	}
	map = create_map();
	if (!set_proof(map, argv[1]))
	{
		write(1, "Error\n", 6);
		return (1);
	}
	if (!brute_fill(map))
	{
		write(1, "Error\n", 6);
		return (1);
	}
	print_map(map);
	free_map(map);
	return (0);
}
