/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 20:31:36 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:21:01 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proof.h"

void	set_proof_col(int **map, int col, int value, int top)
{
	if (top)
		map[4][col] = value;
	else
		map[5][col] = value;
}

int	get_proof_col(int **map, int col, int top)
{
	if (top)
		return (map[4][col]);
	else
		return (map[5][col]);
}

void	set_proof_row(int **map, int row, int value, int left)
{
	if (left)
		map[6][row] = value;
	else
		map[7][row] = value;
}

int	get_proof_row(int **map, int row, int left)
{
	if (left)
		return (map[6][row]);
	else
		return (map[7][row]);
}

int	set_proof(int **map, char *str)
{
	int		i;
	char	c;

	i = 0;
	while (1)
	{
		c = str[i * 2];
		if (c < '1' || c > '4')
			return (0);
		if (i >= 16)
			return (0);
		map[i / 4 + 4][i % 4] = c - '0';
		c = str[i * 2 + 1];
		if (c == '\0')
			return (i == 15);
		if (c != ' ')
			return (0);
		i++;
	}
}
