/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 21:38:00 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:19:26 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H
# define MAP_H 

void	set_row(int **map, int *values, int row);

void	set(int **map, int x, int y, int value);

int		get(int **map, int x, int y);

int		**create_map(void);

#endif
