/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhagenlo <bhagenlo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 19:16:05 by tluik             #+#    #+#             */
/*   Updated: 2021/10/23 18:26:20 by bhagenlo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printer.h"
#include <unistd.h>
#include "map.h"

void	print_single_digit(int i)
{
	char	digit;

	digit = i + '0';
	write(1, &digit, 1);
}

void	print_map(int **map)
{
	int		x;
	int		y;

	y = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			print_single_digit(get(map, x, y));
			if (x < 3)
				write(1, " ", 1);
			x++;
		}
		write(1, "\n", 1);
		y++;
	}
}
