/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/17 04:04:10 by htsang            #+#    #+#             */
/*   Updated: 2021/10/19 16:55:34 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	print_number(int nb)
{
	char	digit;
	int		divider;

	divider = 1;
	while (nb / divider > 9)
	{
		divider *= 10;
	}
	while (divider >= 1)
	{
		digit = nb / divider + '0';
		write(1, &digit, 1);
		nb %= divider;
		divider /= 10;
	}
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-2147483648", 11);
		return ;
	}
	if (nb < 0)
	{
		nb = -nb;
		write(1, "-", 1);
	}
	print_number(nb);
	return ;
}
