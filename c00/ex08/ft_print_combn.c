/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/17 06:09:37 by htsang            #+#    #+#             */
/*   Updated: 2021/10/19 16:17:01 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	print_digits(char i, char numbers[10], int is_fin)
{
	write(1, numbers, 10);
	if (i == '9' && is_fin)
	{
		return ;
	}
	write(1, ", ", 2);
	return ;
}

// b[0] is lower bounds of the while loop
// b[1] is upper bounds of the while loop
void	create_digits(char b[2], char i, char numbers[10], int is_fin)
{
	char	next_b[2];

	next_b[0] = b[0] + 1;
	next_b[1] = b[1] + 1;
	while (i <= b[1])
	{
		numbers[b[0] - '0'] = i;
		if (b[1] == '9')
		{
			print_digits(i, numbers, is_fin);
		}
		else
		{
			create_digits(next_b, i + 1, numbers, i == b[1] && is_fin);
		}
		i++;
	}
	return ;
}

void	empty(char numbers[10])
{
	int		i;

	i = 0;
	while (i <= 9)
	{
		numbers[i] = '\0';
		i++;
	}
	return ;
}

void	ft_print_combn(int n)
{
	char	bounds[2];
	char	numbers[10];

	bounds[0] = '0';
	bounds[1] = '9' + 1 - n;
	if (n <= 0)
	{
		return ;
	}
	empty(numbers);
	create_digits(bounds, '0', numbers, 1);
	return ;
}
