void	n1(void)
{
	char	i;

	i = '0';
	while (i <= '9')
	{
		write(1, &i, 1);
		write(1, " ", 1);
		i++;
	}
	write(1, "\n", 1);
}

void	n2(void)
{
	char	i;

	i = '0';
	while (i <= '8')
	{
		char	j;
		
		j = i + 1;
		while (j <= '9')
		{
			write(1, &i, 1);
			write(1, &j, 1);
			write(1, " ", 1);
			j++;
		}
		i++;
	}
	write(1, "\n", 1);
}