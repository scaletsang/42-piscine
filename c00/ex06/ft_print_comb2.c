/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/17 01:40:09 by htsang            #+#    #+#             */
/*   Updated: 2021/10/19 14:52:41 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	itostr(char digit_c[2], int digit)
{
	digit_c[0] = digit / 10 + '0';
	digit_c[1] = digit % 10 + '0';
	return ;
}

void	ft_print_comb2(void)
{
	int		digit1;
	int		digit2;
	char	digit_c[2];

	digit1 = 0;
	while (digit1 < 98)
	{
		digit2 = digit1 + 1;
		while (digit2 <= 99)
		{
			itostr(digit_c, digit1);
			write(1, digit_c, 2);
			write(1, " ", 1);
			itostr(digit_c, digit2);
			write(1, digit_c, 2);
			write(1, ", ", 2);
			digit2++;
		}
		digit1++;
	}
	write(1, "98 99", 5);
	return ;
}
