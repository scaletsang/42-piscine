/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 09:19:19 by htsang            #+#    #+#             */
/*   Updated: 2021/10/16 16:43:15 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int	main(void)
{
	ft_putchar('G');
	ft_putchar('o');
	ft_putchar('o');
	ft_putchar('d');
	ft_putchar('w');
	ft_putchar('o');
	ft_putchar('r');
	ft_putchar('k');
	ft_putchar('!');
	ft_putchar('\n');
	return (0);
}
