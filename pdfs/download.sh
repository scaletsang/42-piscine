#!/bin/bash
for d in {0..13} ; do
	n=$((20905 + ($d * 8)))
	curl https://cdn.intra.42.fr/pdf/pdf/${n}/en.subject.pdf --output c0${d}.pdf
done
