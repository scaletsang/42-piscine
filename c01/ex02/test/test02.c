/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 16:31:14 by htsang            #+#    #+#             */
/*   Updated: 2021/10/20 16:34:25 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_swap(int *a, int*b);

int	main(void)
{
	int	a;
	int	b;

	a = 24;
	b = 42;
	ft_swap(&a, &b);
	printf("%i %i", a, b);
	return (0);
}
