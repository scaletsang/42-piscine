/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 02:47:03 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 10:45:34 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_rev_int_tab(int *tab, int size)
{
	int	tmp;
	int	*start;
	int	*end;
	int	i;

	start = tab;
	end = tab + size - 1;
	i = 0;
	while (i < (size / 2))
	{
		tmp = *start;
		*start = *end;
		*end = tmp;
		start++;
		end--;
		i++;
	}
	return ;
}
