/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test07.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 02:43:16 by htsang            #+#    #+#             */
/*   Updated: 2021/10/21 02:54:25 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_rev_int_tab(int *tab, int size);

int	main(void)
{
	int	i;
	int	tab[5];

	i = 0;
	tab[0] = 1;
	tab[1] = 2;
	tab[2] = 3;
	tab[3] = 4;
	tab[4] = 5;
	ft_rev_int_tab(tab, 5);
	while (i < 5)
	{
		printf("%i, ", tab[i]);
		i++;
	}
	return (0);
}
