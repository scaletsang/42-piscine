/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 02:29:41 by htsang            #+#    #+#             */
/*   Updated: 2021/10/21 05:51:09 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_ultimate_div_mod(int *a, int *b)
{
	int	a_temp;
	int	b_temp;

	a_temp = *a / *b;
	b_temp = *a % *b;
	*a = a_temp;
	*b = b_temp;
	return ;
}
