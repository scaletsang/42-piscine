/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 03:14:39 by htsang            #+#    #+#             */
/*   Updated: 2021/10/21 05:50:39 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int	i;
	int	*original_tab;
	int	tmp;

	original_tab = tab;
	while (size >= 2)
	{
		i = 0;
		tab = original_tab;
		while (i < (size - 1))
		{
			if (*tab > *(tab + 1))
			{
				tmp = *tab;
				*tab = *(tab + 1);
				*(tab + 1) = tmp;
			}
			tab++;
			i++;
		}
		size--;
	}
	return ;
}
