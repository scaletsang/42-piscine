/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test08.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 02:55:58 by htsang            #+#    #+#             */
/*   Updated: 2021/10/21 05:50:52 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

void	ft_sort_int_tab(int *tab, int size);

int	main(void)
{
	int	arr[1000];
	int	neg;
	int	i;

	neg = -1;
	i = 0;
	while (i < 999)
	{
		arr[i] = rand() * neg % 1000;
		neg *= -1;
		i++;
	}
	i = 0;
	ft_sort_int_tab(arr, 1000);
	while (i < 999)
	{
		printf("%i ", arr[i]);
		i++;
	}
	return (0);
}
