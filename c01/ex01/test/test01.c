/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 13:21:24 by htsang            #+#    #+#             */
/*   Updated: 2021/10/20 15:23:20 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_ultimate_ft(int *********nbr);

int	main(void)
{
	int		val;
	int		*p1;
	int		**p2;
	int		***p3;
	int		****p4;
	int		*****p5;
	int		******p6;
	int		*******p7;
	int		********p8;
	int		*********p9;
	char	str[2];

	p1 = &val;
	p2 = &p1;
	p3 = &p2;
	p4 = &p3;
	p5 = &p4;
	p6 = &p5;
	p7 = &p6;
	p8 = &p7;
	p9 = &p8;
	ft_ultimate_ft(p9);
	str[0] = *********p9 / 10 + '0';
	str[1] = *********p9 % 10 + '0';
	write(2, str, 2);
	return (0);
}
