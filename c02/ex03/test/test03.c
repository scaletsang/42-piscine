/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 07:49:49 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 15:31:22 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_str_is_numeric(char *str);

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while(str[i])
	{
		i++;
	}
	return (i);
}

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, "OK\n", 3);
	}
	else
	{
		write(1, "KO\n", 3);
	}
}

int	test01(void)
{
	write(1, "923456592: ", ft_strlen("923456592: "));
	return (ft_str_is_numeric("923456592") == 1);
}

int	test02(void)
{
	write(1, "278902: ", ft_strlen("278902: "));
	return (ft_str_is_numeric("278902") == 1);
}

int	test03(void)
{
	write(1, "qpxvn23423by&k: ", ft_strlen("qpxvn23423by&k: "));
	return (ft_str_is_numeric("qpxvn23423by&k") == 0);
}

int	test04(void)
{
	write(1, "23478264g9236429: ", ft_strlen("23478264g9236429: "));
	return (ft_str_is_numeric("23478264g9236429") == 0);
}

int	test05(void)
{
	char	*str = NULL;
	write(1, "null pointer: ", 14);
	return (ft_str_is_numeric(str) == 0);
}

int	test06(void)
{
	write(1, "string with space: ", ft_strlen("string with space: "));
	return (ft_str_is_numeric("   ") == 0);
}

int	test07(void)
{
	char	str_p[4] = {0};
	write(1, "pointer to string of 0s: ", ft_strlen("pointer to string of 0s: "));
	return (ft_str_is_numeric(str_p) == 1);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	test(&test07);
	return (0);
}
