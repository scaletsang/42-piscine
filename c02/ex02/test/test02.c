/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 15:29:56 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_str_is_alpha(char *str);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, "OK\n", 3);
	}
	else
	{
		write(1, "KO\n", 3);
	}
}

int	test01(void)
{
	char	str[13] = "kdhgosfishbv";

	write(1, "kdhgosfishbv: ", 14);
	return (ft_str_is_alpha(str) == 1);
}

int	test02(void)
{
	char	str[10] = "abcdefgef";

	write(1, "abcdefgef: ", 11);
	return (ft_str_is_alpha(str) == 1);
}

int	test03(void)
{
	char	str[10] = "qpxvnby&k";

	write(1, "qpxvnby&k: ", 11);
	return (ft_str_is_alpha(str) == 0);
}

int	test04(void)
{
	char	*str = NULL;

	write(1, "null pointer: ", 14);
	return (ft_str_is_alpha(str) == 0);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	return (0);
}
