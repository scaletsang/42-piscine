/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test08.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 07:22:57 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 08:14:23 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strlowcase(char *str);

char	*ft_strcpy(char *dest, char *src, int n)
{
	int		i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	while (i < n)
	{
		dest[i] = '\0';
		i++;
	}
	return (dest);
}

int	main(void)
{
	char	str[50];

	ft_strcpy(str, "i am a lowercase sentence", 50);
	printf("%s => %s\n", "i am a lowercase sentence", ft_strlowcase(str));
	ft_strcpy(str, "i Am SomeTimes a lowerCase seNtence", 50);
	printf("%s => %s\n", "i Am SomeTimes a lowerCase seNtence", ft_strlowcase(str));
	ft_strcpy(str, "I AM A UPPERCASE SENTENCE", 50);
	printf("%s => %s\n", "I AM A UPPERCASE SENTENCE", ft_strlowcase(str));
	ft_strcpy(str, "I @m a Mix6ed 0f Eve&yt(hi*ng. !@#$^&*().<>?/\\|!", 50);
	printf("%s => %s\n", "I @m a Mix6ed 0f Eve&yt(hi*ng. !@#$^&*().<>?/\\|!", ft_strlowcase(str));
	return (0);
}
