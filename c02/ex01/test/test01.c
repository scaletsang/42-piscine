/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/27 10:45:30 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, "OK\n", 3);
	}
	else
	{
		write(1, "KO\n", 3);
	}
}

int	test01(void)
{
	char	*dest;
	char	src[5] = "abcde";
	char	*dest2;
	char	src2[5] = "abcde";
	int		result;

	dest = calloc(5, 1);
	dest2 = calloc(5, 1);
	write(1, strncpy(dest, src, 5), 10);
	write(1, " ", 1);
	write(1, ft_strncpy(dest2, src2, 5), 10);
	write(1, " ", 1);
	result = strcmp(dest, dest2);
	free(dest);
	free(dest2);
	return (result == 0);
}

int	test02(void)
{
	char	*dest;
	char	src[4] = "123\0";
	char	*dest2;
	char	src2[4] = "123\0";
	int		result;

	dest = calloc(10, 1);
	dest2 = calloc(10, 1);
	write(1, strncpy(dest2, src2, 3), 10);
	write(1, " ", 1);
	write(1, ft_strncpy(dest, src, 3), 10);
	write(1, " ", 1);
	result = strcmp(dest, dest2);
	free(dest);
	free(dest2);
	return (result == 0);
}

int	test03(void)
{
	char	dest[5];
	char	src[3] = "123";
	char	dest2[5];
	char	src2[3] = "123";

	write(1, strncpy(dest2, src2, 3), 10);
	write(1, " ", 1);
	write(1, ft_strncpy(dest, src, 3), 10);
	write(1, " ", 1);
	return (strncmp(dest, dest2, 3) == 0);
}

int	test04(void)
{
	char	*dest;
	char	src[3] = "kj\0";
	char	*dest2;
	char	src2[3] = "kj\0";
	int		result;

	dest = calloc(10, 1);
	dest2 = calloc(10, 1);
	write(1, strncpy(dest, src, 5), 10);
	write(1, " ", 1);
	write(1, ft_strncpy(dest2, src2, 5), 10);
	write(1, " ", 1);
	result = strcmp(dest, dest2);
	free(dest);
	free(dest2);
	return (result == 0);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	return (0);
}
