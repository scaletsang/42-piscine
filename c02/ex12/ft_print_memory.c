/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 12:07:08 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 15:45:35 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	to_hex(unsigned long n, char *str, int size)
{
	while (size)
	{
		if ((n & 0xf) <= 9)
		{
			*(str + size - 1) = (n & 0xf) + '0';
		}
		else
		{
			*(str + size - 1) = (n & 0xf) - 10 + 'a';
		}
		n = n >> 4;
		size--;
	}
	return ;
}

void	print_hexaddr(void *addr)
{
	unsigned long	addr_val;
	char			addr_str[16];

	addr_val = (unsigned long) addr;
	to_hex(addr_val, addr_str, 16);
	write(1, addr_str, 16);
	return ;
}

void	print_hexcontent(char *addr)
{
	int		i;
	char	str[2];

	i = 0;
	while (i < 16 && addr[i])
	{
		to_hex((unsigned long) addr[i], str, 2);
		write(1, str, 2);
		if ((i + 1) % 2 == 0)
		{
			write(1, " ", 1);
		}
		i++;
	}
	while (i < 16)
	{
		write(1, "  ", 2);
		if ((i + 1) % 2 == 0)
		{
			write(1, " ", 1);
		}
		i++;
	}
	return ;
}

void	print_content(char *addr)
{
	int	i;

	i = 0;
	while (i < 16 && addr[i])
	{
		if (addr[i] >= 32 && addr[i] <= 126)
		{
			write(1, &addr[i], 1);
		}
		else
		{
			write(1, ".", 1);
		}
		i++;
	}
	return ;
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	void	*start;

	start = addr;
	while (size > 0)
	{
		print_hexaddr(addr);
		write(1, ": ", 2);
		print_hexcontent((char *) addr);
		print_content((char *) addr);
		write(1, "\n", 1);
		addr += 16;
		size--;
	}
	return (start);
}
