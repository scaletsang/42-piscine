/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test12.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 04:01:35 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 06:04:28 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

void	*ft_print_memory(void *addr, unsigned int size);

void	test01(void)
{
	void	*addr;
	char	str[64] = "abcdefghijklmnopabcdefghijklmnopabcdefghijklmnopabcdefghijklmnop";

	addr = str;
	ft_print_memory(addr, 13);
	return ;
}

void	test02(void)
{
	int	val;

	val = 0;
	ft_print_memory((void *)&val, 13);
	return ;
}

void	test03(void)
{
	char	*string;

	string = malloc(16);
	string[4] = 'h';
	string[5] = 'i';
	ft_print_memory(string, 13);
	free(string);
	return ;
}

int	main(void)
{
	test01();
	write(1, "\n", 1);
	test02();
	write(1, "\n", 1);
	test03();
	return (0);
}
