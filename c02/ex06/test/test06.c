/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test06.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 08:13:19 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 07:14:21 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_str_is_printable(char *str);

char	*generate_all_ascii(char strp[128])
{
	int	i;

	i = 1;
	while (i <= 127)
	{
		strp[i - 1] = i;
		i++;
	}
	strp[i] = '\0';
	return (strp);
}

char	*generate_sub_arr(char dest[5], char *src)
{
	int		i;
	char	*original_dest;

	i = 0;
	original_dest = dest;
	while (i < 5)
	{
		*dest = *src;
		dest++;
		src++;
		i++;
	}
	*dest = '\0';
	return (original_dest);
}

int	main(void)
{
	char	ascii_arr[128];
	char	*arr;
	char	subarr[5];
	int		count;

	arr = generate_all_ascii(ascii_arr);
	count = 0;
	while (*(arr + 4) != '\0')
	{
		generate_sub_arr(subarr, arr);
		printf("%s: ", subarr);
		printf("%i\n", ft_str_is_printable(subarr));
		count += ft_str_is_printable(subarr);
		arr++;
	}
	printf("Total amount of ASCII character: 128 (excluding 0)\n");
	printf("Expected amount of printable characters: 128 - 33 = 95\n");
	printf("Expected printable groups: 91\n");
	printf("Amount of printable groups: %i\n", count);
	printf("When string is empty: %i\n", ft_str_is_printable(""));
	return (0);
}
