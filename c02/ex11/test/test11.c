/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test11.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 11:05:11 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 12:06:10 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr_non_printable(char *str);

char	*generate_all_ascii(char strp[128])
{
	int	i;

	i = 1;
	while (i <= 127)
	{
		strp[i - 1] = i;
		i++;
	}
	strp[i] = '\0';
	return (strp);
}

int	main(void)
{
	char	all_ascii[128];

	generate_all_ascii(all_ascii);
	ft_putstr_non_printable(all_ascii);
	return (0);
}
