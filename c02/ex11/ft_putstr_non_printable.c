/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 09:22:24 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 12:06:04 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_is_printable(char c)
{
	if (c >= 32 && c <= 126)
	{
		return (1);
	}
	return (0);
}

char	to_hex(char a)
{
	if (a <= 9)
	{
		return (a + '0');
	}
	return (a - 10 + 'a');
}

void	write_hex(char c)
{
	char	str[2];

	str[0] = to_hex(c / 16);
	str[1] = to_hex(c % 16);
	write(1, str, 2);
	return ;
}

void	ft_putstr_non_printable(char *str)
{
	while (*str)
	{
		if (ft_is_printable(*str))
		{
			write(1, str, 1);
		}
		else
		{
			write(1, "\\", 1);
			write_hex(*str);
		}
		str++;
	}
	return ;
}
