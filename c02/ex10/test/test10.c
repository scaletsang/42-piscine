/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test10.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 09:02:58 by htsang            #+#    #+#             */
/*   Updated: 2021/10/22 09:20:05 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int	main(void)
{
	char	dest[6];
	char	src[6] = "Watsup";
	char	src2[6] = "OhWell";

	printf("result: %s\n", dest);
	ft_strlcpy(dest, src, 7);
	printf("result: %s\n", dest);
	ft_strlcpy(dest, src2, 3);
	printf("result: %s\n", dest);
	return (0);
}
