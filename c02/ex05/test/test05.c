/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test05.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 08:13:19 by htsang            #+#    #+#             */
/*   Updated: 2021/10/21 08:15:10 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_str_is_uppercase(char *str);

int	main(void)
{
	printf("%i\n", ft_str_is_uppercase("AJHGSBCPSDFKHJ"));
	printf("%i\n", ft_str_is_uppercase("kjhbachjghlhgv"));
	printf("%i\n", ft_str_is_uppercase("abcdefghijklmnopqrstuvwxyz"));
	printf("%i\n", ft_str_is_uppercase(""));
	printf("%i\n", ft_str_is_uppercase("    "));
	return (0);
}
