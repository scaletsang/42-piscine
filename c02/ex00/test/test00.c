/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 06:24:14 by htsang            #+#    #+#             */
/*   Updated: 2021/10/25 12:30:41 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char	*ft_strcpy(char *dest, char *src);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, "OK\n", 3);
	}
	else
	{
		write(1, "KO\n", 3);
	}
}

int	test01(void)
{
	char	src[5] = "abcd\0";
	char	dest[5];
	char	dest2[5];

	write(1, "strcpy: ", 8);
	write(1, strcpy(dest2, src), 5);
	write(1, " ft_strcpy: : ", 12);
	write(1, ft_strcpy(dest, src), 5);
	write(1, " ", 1);
	return (strcmp(dest, dest2) == 0);
}

int	test02(void)
{
	char	src[5] = "abcd\0";
	char	dest[10];
	char	dest2[10];

	write(1, "strcpy: ", 8);
	write(1, strcpy(dest2, src), 5);
	write(1, " ft_strcpy: : ", 12);
	write(1, ft_strcpy(dest, src), 5);
	write(1, " ", 1);
	return (strcmp(dest, dest2) == 0);
}

int	main(void)
{
	printf("Test when dest has same size as src:\n");
	test(&test01);
	printf("Test when dest has a bigger size than src:\n");
	test(&test02);
	return (0);
}
