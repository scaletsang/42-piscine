/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 18:57:31 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 19:01:47 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../testsuit/ft_string.h"
#include <unistd.h>

int	main(int argc, char **argv)
{
	int	i;

	i = 1;
	while (i <= argc)
	{
		write(1, argv[i], ft_strlen(argv[i]));
		write(1, "\n", 1);
		i++;
	}
	return (0);
}
