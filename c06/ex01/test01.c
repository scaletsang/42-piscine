/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test08.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 12:04:30 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 16:05:08 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../testsuit/pure_tests.h"

int	ft_ten_queens_puzzle(void);

t_test_result case01(void)
{
	char	*subject = "Test tens puzzle return value";
	t_io	expect;
	t_io	output;

	expect.d = 724;
	output.d = ft_ten_queens_puzzle();
	return (test_result(subject, d, output, expect));
}

int	main(void)
{
	test(&case01);
	return (0);
}
