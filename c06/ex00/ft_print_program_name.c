/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 18:52:17 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 18:55:58 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <../testsuit/ft_string.h>
#include <unistd.h>

int	main(int argc, char **argv)
{
	write(1, argv[1], ft_strlen(argv[1]));
	return (0);
}
