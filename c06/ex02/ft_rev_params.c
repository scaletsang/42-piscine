/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 19:01:52 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 19:03:26 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../testsuit/ft_string.h"
#include <unistd.h>

int	main(int argc, char **argv)
{
	while (argc > 1)
	{
		write(1, argv[argc], ft_strlen(argv[argc]));
		write(1, "\n", 1);
		argc--;
	}
	return (0);
}
