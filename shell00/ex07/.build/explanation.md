find . -regex "^.*~$\|^.*\/\#[^/]+\#$" -delete -print
The regex is seperated into two alternative pattern seperated by a or operator (|):
1. '^.*~$' means matching a pattern starting with 0 or multiple anything(.*) and ends with a ~.
2. '^.*\/\#[^/]+\#$' means matching a pattern starting with 0 or multiple anything(.*), followed by a '/' character, a '#" character, and one or multiple anything that is not a '/' character ([^/]+), and finally ends with a '#' character.
