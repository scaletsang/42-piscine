#!/bin/sh
origin=`pwd`
testdir=./test-clean
mkdircd() {
  mkdir $1 && cd $1
}

[ -d "$testdir" ] && rm -R $testdir

# test folder content starts here
mkdircd $testdir
touch test~ \#test2\#
mkdircd layer01
touch layer01~ \#layer01\# layer01\# \#layer01 ~layer01 layer01
mkdircd \#layer02\#
touch layer02 .layer02
cd ../..
mkdircd layer01b
touch layer01b~~~~~ \#\#layer01b\# layer01b\#\#\# \#\#\#layer01b ~~~~layer01b lay\#er01b lay0~1b la\#yer~01b
# test folder content ends here

cd $origin
