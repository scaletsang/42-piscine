/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 10:29:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/18 17:12:51 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	write_line(int x, char content, char delimiter)
{
	int	i;

	i = x - 2;
	if (x >= 1)
	{
		ft_putchar(delimiter);
	}
	while (i > 0)
	{
		ft_putchar(content);
		i--;
	}
	if (x >= 2)
	{
		ft_putchar(delimiter);
	}
	if (x > 0)
	{
		ft_putchar('\n');
	}
	return ;
}

void	rush(int x, int y)
{
	char	edgev;
	char	edgeh;
	char	up_corner;
	char	bottom_corner;
	int		i;

	edgev = '|';
	edgeh = '-';
	up_corner = 'o';
	bottom_corner = 'o';
	i = y - 2;
	if (y >= 1)
	{
		write_line(x, edgeh, up_corner);
	}
	while (i > 0)
	{
		write_line(x, ' ', edgev);
		i--;
	}
	if (y >= 2)
	{
		write_line(x, edgeh, bottom_corner);
	}
	return ;
}
