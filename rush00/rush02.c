/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 10:29:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/18 17:12:22 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	write_line(int x, char content, char delimiter)
{
	int	i;

	i = x - 2;
	if (x >= 1)
	{
		ft_putchar(delimiter);
	}
	while (i > 0)
	{
		ft_putchar(content);
		i--;
	}
	if (x >= 2)
	{
		ft_putchar(delimiter);
	}
	if (x > 0)
	{
		ft_putchar('\n');
	}
	return ;
}

void	rush(int x, int y)
{
	char	edge;
	char	up_corner;
	char	bottom_corner;
	int		i;

	edge = 'B';
	up_corner = 'A';
	bottom_corner = 'C';
	i = y - 2;
	if (y >= 1)
	{
		write_line(x, edge, up_corner);
	}
	while (i > 0)
	{
		write_line(x, ' ', edge);
		i--;
	}
	if (y >= 2)
	{
		write_line(x, edge, bottom_corner);
	}
	return ;
}
