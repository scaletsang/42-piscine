#!/bin/sh

FILENAME=./all_squares.txt

if [ -d "$FILENAME" ]; then rm $FILENAME; fi
echo "RUSH00:" >> $FILENAME
./rush00 >> $FILENAME
echo "\nRUSH01:" >> $FILENAME
./rush01 >> $FILENAME
echo "\nRUSH02:" >> $FILENAME
./rush02 >> $FILENAME
echo "\nRUSH03:" >> $FILENAME
./rush03 >> $FILENAME
echo "\nRUSH04:" >> $FILENAME
./rush04 >> $FILENAME