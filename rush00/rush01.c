/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/16 10:29:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/18 17:13:00 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	write_line(int x, char content, char delimiter1, char delimiter2)
{
	int	i;

	i = x - 2;
	if (x >= 1)
	{
		ft_putchar(delimiter1);
	}
	while (i > 0)
	{
		ft_putchar(content);
		i--;
	}
	if (x >= 2)
	{
		ft_putchar(delimiter2);
	}
	if (x > 0)
	{
		ft_putchar('\n');
	}
	return ;
}

void	rush(int x, int y)
{
	char	edgev;
	char	up_corner1;
	char	up_corner2;
	int		i;

	edgev = '*';
	up_corner1 = '/';
	up_corner2 = '\\';
	i = y - 2;
	if (y >= 1)
	{
		write_line(x, edgev, up_corner1, up_corner2);
	}
	while (i > 0)
	{
		write_line(x, ' ', edgev, edgev);
		i--;
	}
	if (y >= 2)
	{
		write_line(x, edgev, up_corner2, up_corner1);
	}
	return ;
}
