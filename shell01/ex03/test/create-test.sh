#!/bin/sh
origin=`pwd`
testdir=./test_count_files

# Helper functions:
mkdircd() {
  mkdir $1 && cd $1
}

touches() {
  local dirname=$1
  local i=1

  mkdircd $dirname
  while [ $i -lt $2 ]
  do
    touch $i
    i=`expr $i + 1`
  done
}

[ -d "$testdir" ] && rm -R $testdir

touches $testdir 9
# testdir content starts here

for i in 1 2 3 4
do
  touches "dir$i" 8
done

# testdir content ends here
cd $origin
