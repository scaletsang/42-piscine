#!/bin/sh
origin=`pwd`
testdir=./test_find_sh
mkdircd() {
  mkdir $1 && cd $1
}

[ -d "$testdir" ] && rm -R $testdir
mkdircd $testdir
# testdir content starts here

touch show.se unshow.sh unshow\$.sh show sh
mkdir shsh
mkdircd shh
touch unshow1.sh unshow2.sh shshshsh

# testdir content ends here
cd $origin
