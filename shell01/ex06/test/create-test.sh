#!/bin/sh
origin=`pwd`
testdir=./test_skip

# Helper functions:
mkdircd() {
  mkdir $1 && cd $1
}

[ -d "$testdir" ] && rm -R $testdir

mkdircd $testdir
# testdir content starts here

for i in 1 2 3 4 5 6 7 8 9
do
  touch $i
done

# testdir content ends here
cd $origin
