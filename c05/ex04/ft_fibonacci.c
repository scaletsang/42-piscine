/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 04:53:19 by htsang            #+#    #+#             */
/*   Updated: 2021/10/29 05:22:46 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int calc_fibonacci(int index)
{
	if (index == 0)
	{
		return (0);
	}
	if (index == 1)
	{
		return (1);
	}
	return (calc_fibonacci(index - 1) + calc_fibonacci(index - 2));
}

int	ft_fibonacci(int index)
{
	if (index < 0)
	{
		return (-1);
	}
	return (calc_fibonacci(index));
}
