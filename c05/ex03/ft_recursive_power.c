/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 04:47:22 by htsang            #+#    #+#             */
/*   Updated: 2021/10/29 04:53:14 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	calc_power(int nb, int power)
{
	if (power == 0)
	{
		return (1);
	}
	return (nb * calc_power(nb, power - 1));
}

int	ft_recursive_power(int nb, int power)
{
	if (power < 0)
	{
		return (0);
	}
	return (calc_power(nb, power));
}
