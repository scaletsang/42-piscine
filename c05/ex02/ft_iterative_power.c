/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 04:43:10 by htsang            #+#    #+#             */
/*   Updated: 2021/10/29 04:49:13 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int	accu;

	accu = 1;
	if (power < 0)
	{
		return (0);
	}
	while (power > 0)
	{
		accu *= nb;
		power--;
	}
	return (accu);
}
