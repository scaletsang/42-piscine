/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ten_queens_puzzle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/06 23:14:46 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 14:41:08 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

short	calc_possibilities(int col, char board[10])
{
	short	possibilities;
	int		tmp;
	int		i;

	possibilities = 0;
	i = 0;
	while (i < col)
	{
		tmp = board[i] - '0';
		possibilities |= 1 << tmp;
		tmp += (col - i);
		if (tmp < 10)
		{
			possibilities |= 1 << tmp;
		}
		tmp -= (col - i) * 2;
		if (tmp >= 0)
		{
			possibilities |= 1 << tmp;
		}
		i++;
	}
	return (possibilities);
}

int	backtrace(int col, int count, char board[10])
{
	short	possibilities;
	char	i;

	if (col == 10)
	{
		write(1, board, 10);
		write(1, "\n", 1);
		return (count + 1);
	}
	possibilities = calc_possibilities(col, board);
	i = '0';
	while (i <= '9')
	{
		if ((possibilities & 1) == 0)
		{
			board[col] = i;
			count = backtrace(col + 1, count, board);
		}
		possibilities >>= 1;
		i++;
	}
	return (count);
}

int	ft_ten_queens_puzzle(void)
{
	char	board[10];

	return (backtrace(0, 0, board));
}
