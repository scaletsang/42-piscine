/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test06.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 12:04:30 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 17:35:25 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../testsuit/pure_tests.h"

int	ft_is_prime(int nb);

t_test_result case01(void)
{
	char	*subject = "Test if 9 is prime";
	t_io	expect;
	t_io	output;

	expect.d = 0;
	output.d = ft_is_prime(9);
	return (test_result(subject, d, output, expect));
}

int	main(void)
{
	test(&case01);
	return (0);
}
