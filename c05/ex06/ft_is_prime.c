/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 05:53:07 by htsang            #+#    #+#             */
/*   Updated: 2021/11/05 23:59:07 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	factor;

	if (nb % 2 == 0)
	{
		return (0);
	}
	factor = 3;
	while (factor < nb)
	{
		if (nb % factor == 0)
		{
			return (0);
		}
		factor += 2;
	}
	return (1);
}
