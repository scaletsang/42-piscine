/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/06 15:20:53 by htsang            #+#    #+#             */
/*   Updated: 2021/11/06 23:11:58 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	factor;

	if (nb % 2 == 0)
	{
		return (0);
	}
	factor = 3;
	while (factor < nb)
	{
		if (nb % factor == 0)
		{
			return (0);
		}
		factor += 2;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	while (!ft_is_prime(nb) && nb < 2147483647)
	{
		nb++;
	}
	return (nb);
}
