/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test07.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 12:04:30 by htsang            #+#    #+#             */
/*   Updated: 2021/11/07 18:15:39 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../testsuit/pure_tests.h"

int	ft_find_next_prime(int nb);

t_test_result case01(void)
{
	char	*subject = "test next prime after 9";
	t_io	expect;
	t_io	output;

	expect.d = 11;
	output.d = ft_find_next_prime(9);
	return (test_result(subject, d, output, expect));
}

t_test_result case02(void)
{
	char	*subject = "test negative prime";
	t_io	expect;
	t_io	output;

	expect.d = -1031;
	output.d = ft_find_next_prime(-1032);
	return (test_result(subject, d, output, expect));
}

int	main(void)
{
	test(&case01);
	test(&case02);
	return (0);
}
