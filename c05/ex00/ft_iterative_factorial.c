/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 18:00:36 by htsang            #+#    #+#             */
/*   Updated: 2021/10/29 04:37:49 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int	accu;

	accu = 1;
	if (nb < 0)
	{
		return (0);
	}
	while (nb > 1)
	{
		accu *= nb;
		nb--;
	}
	return (accu);
}
