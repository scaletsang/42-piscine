# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    main.c.template                                    :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/25 06:45:20 by htsang            #+#    #+#              #
#    Updated: 2021/10/25 19:17:04 by htsang           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#include <unistd.h>
#include <string.h>
#include <stdio.h>


void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{

	return (0);
}

int	main(void)
{
	test(&test01);
	return (0);
}
