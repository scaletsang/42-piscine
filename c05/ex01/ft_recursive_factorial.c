/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 04:37:53 by htsang            #+#    #+#             */
/*   Updated: 2021/10/29 04:42:44 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	calc_factorial(int nb)
{
	if (nb == 0)
	{
		return (1);
	}
	return (nb * calc_factorial(nb - 1));
}

int	ft_recursive_factorial(int nb)
{
	if (nb < 0)
	{
		return (0);
	}
	return (calc_factorial(nb));
}
