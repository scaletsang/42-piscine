#!/bin/bash

WORKSPACE_DIR=$HOME/workspace
DEST_DIR=$HOME/upload

GREEN='\033[0;32m'
RED='\033[0;31m'
WHITE='\033[0m'

echo -e $GREEN"Start writing your projects to the upload area"$WHITE
cp -Rf $WORKSPACE_DIR/* $DEST_DIR
echo -e $GREEN"Finished writing."$WHITE
echo -e $RED"Converting git submodules to normal folders."$WHITE
rm -Rf `find $DEST_DIR/*/ -type d -name ".git" -o -name ".gitignore"`
echo -e $GREEN"Finished cloning."$WHITE