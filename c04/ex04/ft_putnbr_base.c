/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 17:43:00 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 16:42:16 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
	{
		len++;
	}
	return (len);
}

int	valid_base(char *base)
{
	int	i;
	int	j;

	i = 0;
	if (base == 0)
		return (0);
	while (base[i])
	{
		if (base[i] == '-' || base[i] == '+')
			return (0);
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	return (i > 1);
}

void	print_number(int nb, int limit, char *base)
{
	int	divider;

	divider = 1;
	while (nb / divider <= -limit)
	{
		divider *= limit;
	}
	while (divider >= 1)
	{
		write(1, &base[nb / divider * -1], 1);
		nb %= divider;
		divider /= limit;
	}
	return ;
}

void	ft_putnbr_base(int nbr, char *base)
{
	if (!valid_base(base))
	{
		return ;
	}
	if (nbr < 0)
	{
		write(1, "-", 1);
	}
	else
	{
		nbr = -nbr;
	}
	print_number(nbr, ft_strlen(base), base);
	return ;
}
