/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 16:34:21 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

void	ft_putnbr_base(int nbr, char *base);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

void	write_when(int has_space)
{
	if (has_space)
	{
		write(1, " ", 1);
	}
	return ;
}

int	general_test(char *base, int has_space)
{
	int i;

	i = -100;
	ft_putnbr_base(-2147483648, base);
	write_when(has_space);
	ft_putnbr_base(2147483647, base);
	write_when(has_space);
	while (i <= 100)
	{
		ft_putnbr_base(i, base);
		write_when(has_space);
		i++;
	}
	return (1);
}

int	test01(void)
{
	return (general_test("0123456789", 1));
}

int	test02(void)
{
	return (general_test("01", 1));
}



int	test03(void)
{
	return (general_test("0123456789ABCDEF", 1));
}

int	test04(void)
{
	return (general_test("poneyvif", 1));
}

int	test05(void)
{
	return (general_test("abcdefghijklmonpqrstuvwxyz", 1));
}

int	test06(void)
{
	
	return (general_test("", 0));
}

int	test07(void)
{
	return (general_test("1786623", 0));
}

int	test08(void)
{
	return (general_test("skjfgh+s", 0));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	test(&test07);
	test(&test08);
	return (0);
}
