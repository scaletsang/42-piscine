/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/27 06:37:42 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

void	ft_putstr(char *str);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{
	char	*str = "abcdefgghsi";

	write(1, str, strlen(str));
	write(1, " ", 1);
	ft_putstr(str);
	return (1);
}

int	test02(void)
{
	char	*str = "";

	write(1, str, strlen(str));
	write(1, " ", 1);
	ft_putstr(str);
	return (1);
}

int	test03(void)
{
	char	*str = "&*$v72nfwn896*(^#b9fsdfhgsdjkfgsdlkyv89efbwof7iwb0f724r62b sdkjfgwiod7t346thweofg";

	write(1, str, strlen(str));
	write(1, " ", 1);
	ft_putstr(str);
	return (1);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	return (0);
}
