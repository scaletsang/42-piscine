/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test05.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 16:51:25 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

int	ft_atoi_base(char *str, char *base);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

void	print_number(int nb)
{
	char	digit;
	int		divider;

	divider = 1;
	while (nb / divider > 9)
	{
		divider *= 10;
	}
	while (divider >= 1)
	{
		digit = nb / divider + '0';
		write(1, &digit, 1);
		nb %= divider;
		divider /= 10;
	}
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-2147483648", 11);
		return ;
	}
	if (nb < 0)
	{
		nb = -nb;
		write(1, "-", 1);
	}
	print_number(nb);
	return ;
}

void	print_error(int result, int answer)
{
	write(1, "Failed with the result: ", 24);
	ft_putnbr(result);
	write(1, " Expected result: ", 18);
	ft_putnbr(answer);
}

int	general_test(char *str, char *base, int answer)
{
	int result;

	result = ft_atoi_base(str, base);
	if (result != answer)
	{
		print_error(result, answer);
		return (0);
	}
	ft_putnbr(result);
	write(1, " ", 1);
	return(1);
}

int	test01(void)
{
	char	*base = "0123456789";
	char	*str1 = "   ---+--+1234ab567";
	int		answer1 = -1234;
	char	*str2 = "-2147483648";
	int		answer2 = -2147483648;
	char	*str3 = "2147483647";
	int		answer3 = 2147483647;
	char	*str4 = "\f\n\r\t\v 12539113";
	int		answer4 = 12539113;
	char	*str5 = "\f\n\r\t\v 12539113234234";
	int		answer5 = (int) 12539113234234;
	char	*str6 = "\f\n\r\t\v -82753492389254342";
	int		answer6 = (int) -82753492389254342;
	char	*str7 = "\f\n\r  +-\t\v 12539113";
	int		answer7 = 0;
	char	*str8 = "shgdfl";
	int		answer8 = 0;

	return (general_test(str1, base, answer1)
		&& general_test(str2, base, answer2)
		&& general_test(str3, base, answer3)
		&& general_test(str4, base, answer4)
		&& general_test(str5, base, answer5)
		&& general_test(str6, base, answer6)
		&& general_test(str7, base, answer7)
		&& general_test(str8, base, answer8));
}

int	test02(void)
{
	char	*base = "01";
	char	*str1 = "   ---+--+1001101001023";
	int		answer1 = -1234;
	char	*str2 = "-10000000000000000000000000000000";
	int		answer2 = -2147483648;
	char	*str3 = "1111111111111111111111111111111";
	int		answer3 = 2147483647;
	char	*str4 = "\f\n\r\t\v 101111110101010011101001";
	int		answer4 = 12539113;
	char	*str5 = "\f\n\r\t\v 10110110011101111101011000111101111100111010";
	int		answer5 = (int) 12539113234234;
	char	*str6 = "\f\n\r\t\v -100100101111111111101110011100101110010001101010011000110";
	int		answer6 = (int) -82753492389254342;
	char	*str7 = "\f\n\r  +-\t\v 101111110101010011101001";
	int		answer7 = 0;
	char	*str8 = "shgdfl";
	int		answer8 = 0;

	return (general_test(str1, base, answer1)
		&& general_test(str2, base, answer2)
		&& general_test(str3, base, answer3)
		&& general_test(str4, base, answer4)
		&& general_test(str5, base, answer5)
		&& general_test(str6, base, answer6)
		&& general_test(str7, base, answer7)
		&& general_test(str8, base, answer8));
}

int	test03(void)
{
	char	*base = "0123456789abcdef";
	char	*str1 = "   ---+--+4d2plk";
	int		answer1 = -1234;
	char	*str2 = "-80000000";
	int		answer2 = -2147483648;
	char	*str3 = "7fffffff";
	int		answer3 = 2147483647;
	char	*str4 = "\f\n\r\t\v bf54e9";
	int		answer4 = 12539113;
	char	*str5 = "\f\n\r\t\v b677d63df3a";
	int		answer5 = (int) 12539113234234;
	char	*str6 = "\f\n\r\t\v -125ffdce5c8d4c6";
	int		answer6 = (int) -82753492389254342;
	char	*str7 = "\f\n\r  +-\t\v bf54e9";
	int		answer7 = 0;
	char	*str8 = "shgdfl";
	int		answer8 = 0;

	return (general_test(str1, base, answer1)
		&& general_test(str2, base, answer2)
		&& general_test(str3, base, answer3)
		&& general_test(str4, base, answer4)
		&& general_test(str5, base, answer5)
		&& general_test(str6, base, answer6)
		&& general_test(str7, base, answer7)
		&& general_test(str8, base, answer8));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	return (0);
}
