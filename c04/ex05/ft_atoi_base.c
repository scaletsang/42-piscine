/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:20:55 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 14:43:42 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	valid_base(char *base)
{
	int	i;
	int	j;

	i = 0;
	if (base == 0)
		return (0);
	while (base[i])
	{
		if (base[i] == '-' || base[i] == '+')
			return (0);
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	return (i > 1);
}

int	find_in_base(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
		{
			return (i);
		}
		i++;
	}
	return (-1);
}

int	make_number(char *str, char *base)
{
	int		val;
	int		base_length;
	long	number;

	base_length = 0;
	number = 0;
	while (base[base_length])
	{
		base_length++;
	}
	val = find_in_base(*str, base);
	while (val != -1)
	{
		number *= base_length;
		number += val;
		str++;
		val = find_in_base(*str, base);
	}
	return (number);
}

int	ft_atoi_base(char *str, char *base)
{
	int		sign;

	sign = 1;
	if (!valid_base(base))
	{
		return (0);
	}
	while (*str == ' ' || *str == '\f' || *str == '\n'
		|| *str == '\r' || *str == '\t' || *str == '\v')
	{
		str++;
	}
	while (*str == '+' || *str == '-')
	{
		if (*str == '-')
		{
			sign *= -1;
		}
		str++;
	}
	return ((int)(make_number(str, base) * sign));
}
