/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/27 06:32:30 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

int	ft_strlen(char *str);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{
	char	*str = "abcdeskdhfgsdl";

	return (ft_strlen(str) == (int) strlen(str));
}

int	test02(void)
{
	char	*str = "";

	return (ft_strlen(str) == (int) strlen(str));
}

int	test03(void)
{
	char	*str = "abcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdlabcdeskdhfgsdl";

	return (ft_strlen(str) == (int) strlen(str));
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	return (0);
}
