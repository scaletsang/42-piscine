/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 09:24:04 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

int	ft_atoi(char *str);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
}

void	print_number(int nb)
{
	char	digit;
	int		divider;

	divider = 1;
	while (nb / divider > 9)
	{
		divider *= 10;
	}
	while (divider >= 1)
	{
		digit = nb / divider + '0';
		write(1, &digit, 1);
		nb %= divider;
		divider /= 10;
	}
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-2147483648", 11);
		return ;
	}
	if (nb < 0)
	{
		nb = -nb;
		write(1, "-", 1);
	}
	print_number(nb);
	return ;
}

int	test01(void)
{
	char	*str = "   ---+--+1234ab567";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == -1234);
}

int	test02(void)
{
	char	*str = "-2147483648";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == -2147483648);
}

int	test03(void)
{
	char	*str = "2147483647";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == 2147483647);
}

int	test04(void)
{
	char	*str = "\f\n\r\t\v 12539113";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == 12539113);
}

int	test05(void)
{
	char	*str = "\f\n\r  +-\t\v 12539113";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == 0);
}

int	test06(void)
{
	char	*str = "\f\n\r\t\v 12539113234234";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == (int) 12539113234234);
}

int	test07(void)
{
	char	*str = "\f\n\r\t\v -82753492389254342";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == (int) -82753492389254342);
}

int	test08(void)
{
	char	*str = "shgdfl";

	ft_putnbr(ft_atoi(str));
	return (ft_atoi(str) == 0);
}

int	main(void)
{
	test(&test01);
	test(&test02);
	test(&test03);
	test(&test04);
	test(&test05);
	test(&test06);
	test(&test07);
	test(&test08);
	return (0);
}
