/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 06:45:20 by htsang            #+#    #+#             */
/*   Updated: 2021/10/28 12:45:12 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <stdio.h>

void	ft_putnbr(int nb);

void	test(int (*func)(void))
{
	if ((*func)())
	{
		write(1, " OK\n", 4);
	}
	else
	{
		write(1, " KO\n", 4);
	}
	return ;
}

int	test01(void)
{
	int	i;

	i = -50;
	while (i <= 50)
	{
		ft_putnbr(i);
		write(1, " ", 1);
		i++;
	}
	ft_putnbr(-2147483648);
	write(1, " ", 1);
	ft_putnbr(2147483647);
	write(1, "\n", 1);
	return (1);
}

int	main(void)
{
	test(&test01);
	return (0);
}
